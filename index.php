<?php

require __DIR__."/vendor/autoload.php";

use CoffeeCode\Router\Router;

$router = new Router(URL_BASE);

/**
 * Controllers
 */
$router->namespace("Source\App");

/**
 * Web
 * home
 */
$router->group(null);
$router->get("/","Web:home");
$router->get("/contato","Web:contact");


/**
 * contato
 */
$router->group("contato");
$router->get("/","Web:contact");
$router->post("/","Web:contact");
$router->delete("/","Web:contact");
$router->get("/{sector}","Web:contact");

/**
 * Admin
 * home
 */
$router->group("admin");
$router->get("/","Admin:home");



/**
 * ERROS
 */
$router->group("ops");
$router->get("/{errcode}","Web:error");

$router->dispatch();

if($router->error()){
    $router->redirect("/ops/{$router->error()}");
}
?>